
## Avengers Team: Cheapoair.com Automation


## Table of Contents
1. General Info 
2. Technologies
3. Installation
4. Collaboration

### General Info
***
The following project is a Automation project, only for testing purposes by implementing Selenium webDriver technologies and Java. An ongoing project which progress is around 85 ~ 90% ... still adjusting it.

Dev Team: TM Avengers Team 

@Nestor Gonzalez
@Andres Avila

---

## Technologies
***
A list of technologies used within the project:
* [Java]: java version "15.0.2" 2021-01-19
* [Java(TM) SE Runtime Environment]: (build 15.0.2+7-27)
* [JDK]: 15.0.2

Selenium WebDriver
* [Selenium WebDriver]: Version 3.141.59
* [ChromeDriver]: Version chromedriver_win32_91

* [TestNG]: Version 6.14.3
* [Extent Reports]: Version 5.0.8
* [Commons IO]: Version commons-io
* [Apache POI]: Version 5.0.0

* [Repo]: Bitbucket

---
