package com.cheapoair.pageObjectModel;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class TravelResultsPage {
	
	WebDriver driver;
	
	//Constructor
	public void loginPage() {
		this.driver = driver;
	}
	
	
	//Locators
	@FindBy (how=How.XPATH, using ="//*[@value='Modify Search']") WebElement modifySearchBtn;
	@FindBy (how=How.XPATH, using ="//span[@class='trip__airline--name']") List<WebElement> airlineNames;
	@FindBy (how=How.LINK_TEXT, using ="select another airline") WebElement alertMessageSrchALine;
	@FindBy (how=How.XPATH, using="//div[@class='breadscrum']/section/span") WebElement filterElement;
	//MultiTrip
	@FindBy (how=How.XPATH, using="//*[@placeholder='Where from?']") List<WebElement> multiTripResultsQ;

	
	//Get Methods
	public WebElement getWebElementModifySearchBtn() {
		return modifySearchBtn;
	}
	
	public WebElement getAlertNotFound() {
		return alertMessageSrchALine;
	}
	
	public WebElement getFilterElement() {
		return filterElement;
	}
	
	public List<WebElement> getListResults() {
		return airlineNames;
	}
	
	public List<WebElement> getMultiTripList(){
		return multiTripResultsQ;
	}
	
	public int getAirlinesList(String airlineOption) {
		int countFilter = 0;
		for(WebElement option :airlineNames) {
			if(option.getText().equalsIgnoreCase(airlineOption)) {
				countFilter++;				
			}
		}
		return countFilter;
	}	

}


