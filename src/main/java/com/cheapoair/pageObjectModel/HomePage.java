package com.cheapoair.pageObjectModel;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class HomePage {
	
	public WebDriver driver;
	
	
	//Locators - WebElements
	By currency = By.cssSelector("a[class='utility__link dropdown-toggle currency-arrow']");
	By inr = By.partialLinkText("INR");
	By flights = By.cssSelector("a[title='Flights']");
	By flightbutton = By.cssSelector("a[title='Flights']");
	By hotelbutton = By.cssSelector("a[title='Hotels']");
	By Carsbutton = By.cssSelector("a[title='Cars']");
	By Packagebutton = By.cssSelector("a[title='Packages']");
	By oneway = By.cssSelector("label[for='onewayTrip']");
	By onewaytrip = By.id("to0");
	By done = By.id("closeDealDialog");
	By rango = By.cssSelector("div[class='widget']");
	By multi = By.cssSelector("label[for='multiTrip']");
	By range2 = By.cssSelector("div[class='col-12 col-md-12 col-lg-9 col-left']");
	By searchmulti = By.cssSelector("input[class='btn btn-lg btn-block btn-default btn-search']");

	//SignIn Section
	@FindBy (how=How.LINK_TEXT, using ="Sign In") WebElement signInBtn;
	@FindBy (how=How.XPATH, using ="//a/strong[@class='text-primary']") WebElement successLogInTxt;
	@FindBy (how=How.XPATH, using ="//*[@id='flights']") WebElement flightsLinkA;
	@FindBy (how=How.ID, using="roundTrip") WebElement roundTripRadio;
	@FindBy (how=How.XPATH, using="//input[@id='from0']") WebElement originFld;
	@FindBy (how=How.XPATH, using="//input[@id='to0']") WebElement destinationFld;
	@FindBy (how=How.XPATH, using="//input[@id='to1']") WebElement destinationFldB;
	@FindBy (how=How.XPATH, using="//*[@id='from0']/parent::div/a" ) WebElement clrOriginFld;
	@FindBy (how=How.ID, using="vacations") WebElement packagesBtn;
	@FindBy (how=How.XPATH, using="//*[@class='member-deals__discountfare d-inline ']/span[2]") List<WebElement> pckPrices;
	//Round Trip WebElements to1
	@FindBy (how=How.XPATH, using="//input[@id='from0']/parent::div/section/div/ul/li") List<WebElement> sgOriginOptions;
	@FindBy (how=How.XPATH, using="//input[@id='to0']/parent::div/section/div/ul/li") List<WebElement> sgDestinationOptions;
	@FindBy (how=How.XPATH, using="//input[@id='to1']/parent::div/section/div/ul/li") List<WebElement> sgDestinationOptionsB;
	@FindBy (how=How.XPATH, using="//*[@id='cal0']/following-sibling::div/header/following-sibling::nav/a[2]") WebElement nextMonth;
	@FindBy (how=How.XPATH, using="//*[@id='cal1']/following-sibling::div/header/following-sibling::nav/a[2]") WebElement nextMonth2;
	@FindBy (how=How.XPATH, using="//*[@id='cal0']/following-sibling::div/header") WebElement calendarCurrentMonth;
	@FindBy (how=How.XPATH, using="//*[@id='cal1']/following-sibling::div/header") WebElement calendarCurrentMonthB;	
	@FindBy (how=How.XPATH, using="//a[@class=' month-date']") List<WebElement> activeMonthDays1;
	@FindBy (how=How.ID, using="travellerButton") WebElement travelerCoach;
	@FindBy (how=How.ID, using="addadults") WebElement trCoachAddAdults;
	@FindBy (how=How.ID, using="addseniors") WebElement trCoachAddSeniors;
	@FindBy (how=How.ID, using="addchild") WebElement trCoachAddChilds;
	@FindBy (how=How.ID, using="addinfant") WebElement trCoachAddInfants;
	@FindBy (how=How.ID, using="addlapinfant") WebElement trCoachAddLapInf;
	@FindBy (how=How.XPATH, using="//select[@name='Class']") WebElement classDropDown;
	@FindBy (how=How.XPATH, using="//*[@id='Class']/option") List<WebElement> classOptions;
	@FindBy (how=How.ID, using="closeDialog") WebElement closeDialogBtn;
	@FindBy (how=How.ID, using="searchNow") WebElement searchNowBtn;
	@FindBy (how=How.XPATH, using="//*[@placeholder='Search Preferred Airline']") WebElement searchPreferredAirline;
	@FindBy (how=How.XPATH, using="//section[@class='suggestion-box']/div/ul/li") List<WebElement> airLineOptions;
	@FindBy (how=How.XPATH, using="//label[@for='multiTrip']") WebElement multiCityRadio;
	@FindBy (how=How.XPATH, using="//a[@aria-label='Delete Flight']") WebElement removeExtraFlight;
	
	//Constructor
	public HomePage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver=driver;
	}


	//Methods
	public WebElement getCurrency()
	{
		return driver.findElement(currency);
	}
	
	public WebElement getINR()
	{
		return driver.findElement(inr);
	}
	
	public WebElement getFlight()
	{
		return driver.findElement(flights);
	}

	public WebElement getflightbutton()
	{
		return driver.findElement(flightbutton);
	}
	
	public WebElement getHotelButton()
	{
		return driver.findElement(hotelbutton);
	}
	
	public WebElement getCarsButton()
	{
		return driver.findElement(Carsbutton);
	}
	
	public WebElement getPackageButton()
	{
		return driver.findElement(Packagebutton);
	}
	
	public WebElement getOneWayTrip()
	{
		return driver.findElement(oneway);
	}
	
	public WebElement getOneWayTripText()
	{
		return driver.findElement(onewaytrip);
	}
	

	public WebElement getDoneButton()
	{
		return driver.findElement(done);
	}
	
	public WebElement getOneWayRange()
	{
		return driver.findElement(rango);
	}
	
	public WebElement getMulti()
	{
		return driver.findElement(multi);
	}
	
	public WebElement getMultiWayRange()
	{
		return driver.findElement(range2);
	}

	//Click Methods
	public void clickOnSignIn() {
		signInBtn.click();
	}
	
	public void clickOnFlightsSearch() {
		flightsLinkA.click();
	}
	
	public void clickOnRoundTripRadio() {
		roundTripRadio.click();
	}
	public void clickOnClearFieldOrigin() {
		clrOriginFld.click();
	}
	
	public void clickNextMonth() {
		nextMonth.click();
	}
	
	public void clickTravCoach() {
		travelerCoach.click();
	}
	
	public void clicktrCoachAddAdults() {
		trCoachAddAdults.click();
	}
	
	public void clicktrCoachAddSeniors() {
		trCoachAddSeniors.click();
	}
	
	public void clicktrCoachAddChilds() {
		trCoachAddChilds.click();
	}
	
	public void clicktrCoachAddInfants() {
		trCoachAddInfants.click();
	}
	
	public void clicktrCoachAddLapInf() {
		trCoachAddLapInf.click();
	}
	
	public void clickClassDropDown() {
		classDropDown.click();
	}
	
	public void clickPackages() {
		packagesBtn.click();
	}
	public void clickCoachMenu(int adult, int senior, int child, int seatInf, int lapInf, String classInput) {
		int initialValue = 1;
		int defaultValue = 0;
		while(initialValue < adult) {
			clicktrCoachAddAdults();
			initialValue++;
		}
		
		while(defaultValue < senior) {
			clicktrCoachAddSeniors();
			defaultValue++;
		}
		defaultValue=0;
		while(defaultValue < child) {
			clicktrCoachAddChilds();
			defaultValue++;
		}
		defaultValue=0;
		while(defaultValue < seatInf) {
			clicktrCoachAddInfants();
			defaultValue++;
		}
		defaultValue=0;
		while(defaultValue < lapInf) {
			clicktrCoachAddLapInf();
			defaultValue++;
		}
		
		classDropDown.click();
		for(WebElement option :classOptions) {
			if(option.getText().equalsIgnoreCase(classInput)) {
				option.click();
				closeDialogBtn.click();
				break;
			}
		}
		System.out.println("Travelers details selection... ");
	}
	
	public void clickAirlineOptions(String desiredAirLine) {
		for(WebElement option :airLineOptions) {
			if(option.getText().equalsIgnoreCase(desiredAirLine)) {
				option.click();
				break;
			}
		}
		
	}
	
	public void sortClickSuggestiveList(List<WebElement> listName, String originChoice){ //Method to sort lists generated for Autosuggestive menus
		for(WebElement option :listName) {
			if(option.getText().equals(originChoice)) {
				option.click();
				break;
			}
		}
	}	
	
	public void clickMonthDesired(String monthDesired, String desiredDepartDay){
		int currentMonth = sortingMonths(getCurrentMonth());
		int desiredMonth = sortingMonths(monthDesired);
		int dif=1;
		System.out.println("Main Flight date selection...");
		if(currentMonth==desiredMonth) {
			//System.out.println("meses iguales");
			clickDayDesired(activeMonthDays1, desiredDepartDay);
		}else {
			if(desiredMonth > currentMonth) {
				dif = desiredMonth - currentMonth;				
				for(int i=1; i<=dif; i++) { //Checks month for current year
					nextMonth.click();
				}
			}else {
				dif = 7 + desiredMonth+1; 
				for(int i=1; i<=dif; i++) { //Checks month for next year
					nextMonth.click();
				}				
			}
			clickDayDesired(activeMonthDays1, desiredDepartDay);
		}
	}
	
	public void clickMonthDesiredB(String monthDesired, String desiredDepartDay, String currentMonth) {
		int cMonth = sortingMonths(currentMonth)+1;
		int dMonth = sortingMonths(monthDesired)+1;
		int dif=1;
		System.out.println("Second flight: Date selection...");
		if(cMonth==dMonth) {
			clickDayDesired(activeMonthDays1, desiredDepartDay);
		}else {
			if(dMonth > cMonth) {
				dif = dMonth - cMonth;	
				for(int i=1; i<=dif; i++) { //Checks month for current year
					nextMonth2.click();
				}
				
			}else {
				System.out.println("entro acá????s");
				dif = 7 + dMonth+1; 
				for(int i=1; i<=dif; i++) { //Checks month for next year
					nextMonth2.click();
				}				
			}
			clickDayDesired(activeMonthDays1, desiredDepartDay);
		}
		
		
	}
	
	public void clickDayDesired(List<WebElement> listDays, String desiredDay) {
		for(WebElement day :listDays) {
			if(day.getText().equals(desiredDay)) {
				day.click();
			}
		}
	}
	
	public void clickSearchBtn() {
		searchNowBtn.click();
	}
	
	public void clickMultiCity() {
		multiCityRadio.click();
	}
	
	public void clickCloseFlight() {
		removeExtraFlight.click();
	}
	
	//Get Methods
	public String getSuccessLogInTxt() {
		String successTxt = successLogInTxt.getText();
		return successTxt;		
	}
	
	public List<WebElement> getOriginOptions() {
		return sgOriginOptions;
	}	
	
	public List<WebElement> getPckPrices(){
		return pckPrices;
	}
	
	public List<WebElement> getDestinationOptions(){
		return sgDestinationOptions;
	}
	
	public List<WebElement> getDestinationOptionsB(){
		return sgDestinationOptionsB;
	}
	
	public String getCurrentMonth() {
		String currentMonth = calendarCurrentMonth.getText().split("Today is ")[1].strip().split(" ")[0];
		return currentMonth;
	}
	public String getCurrentMonthB() {
		String currentMonth = calendarCurrentMonthB.getText().split("Today is ")[1].strip().split(" ")[0];
		return currentMonth;
	}

	public WebElement getMultiWaysearch()
	{
		return driver.findElement(searchmulti);
	}

	
	//Action Methods
	public void setOriginFld(String locationTxt) {
		originFld.sendKeys(locationTxt);
	}
	
	public void setDestinationFld(String toTxt) {
		destinationFld.sendKeys(toTxt);
	}
	public void setDestinationFildB(String toTxtB) {
		destinationFldB.sendKeys(toTxtB);
	}
	
	public void setAirlineSrch(String airline) {
		searchPreferredAirline.sendKeys(airline);
	}
	
	
	//Methods utilities
	public boolean isCheckedRoundTrip() {
		boolean check = roundTripRadio.isSelected();		
		return check;
	}
	
	public int sortingMonths(String monthInput) {
		String[] months = {"January", "February","March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
		int monthIndex = 0;
		for(int i=0; i<months.length; i++) {
			//if(monthInput.equalsIgnoreCase(months[i])) {
			if(months[i].contains(monthInput)) {	
				monthIndex = i;
				break;
			}
		}		
		return monthIndex;
	}
	
	//Exceptions
	
}
