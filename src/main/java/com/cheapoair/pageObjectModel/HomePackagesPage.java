package com.cheapoair.pageObjectModel;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class HomePackagesPage {
	
	public WebDriver driver;
	
	//Constructor
	public HomePackagesPage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver=driver;
	}
	
	//Locators
	@FindBy(how = How.ID, using="from") WebElement fromField;
	@FindBy(how = How.ID, using="to") WebElement toField;
	@FindBy(how = How.XPATH, using="//input[@id='from']/parent::div/section/div/ul/li") List<WebElement> resultsFrom;
	@FindBy(how = How.XPATH, using="//input[@id='to']/parent::div/section/div/ul/li") List<WebElement> resultsTo;
	@FindBy (how=How.XPATH, using="//*[@id='cal0']/following-sibling::div/header") WebElement calendarCurrentMonth;
	@FindBy (how=How.XPATH, using="//a[@class=' month-date']") List<WebElement> activeMonthDays1;
	@FindBy (how=How.XPATH, using="//*[@id='cal0']/following-sibling::div/header/following-sibling::nav/a[2]") WebElement nextMonth;
	@FindBy (how=How.ID, using="vacationSearchBtn") WebElement searchPckBtn;
	@FindBy(how=How.ID, using="Price") WebElement costsFilter;
	
	//Methods
	
	//Action Methods
	public void setFromTxt(String fromTxt) {
		fromField.sendKeys(fromTxt);
	}
	
	public void setToTxt(String toTxt) {
		toField.sendKeys(toTxt);
	}
	
	public void clickMonthDesired(String monthDesired, String desiredDepartDay){
		int currentMonth = sortingMonths(getCurrentMonth());
		int desiredMonth = sortingMonths(monthDesired);
		int dif=1;
		if(currentMonth==desiredMonth) {
			//System.out.println("meses iguales");
			clickDayDesired(activeMonthDays1, desiredDepartDay);
		}else {
			if(desiredMonth > currentMonth) {
				dif = desiredMonth - currentMonth;				
				for(int i=1; i<=dif; i++) { //Checks month for current year
					nextMonth.click();
				}
			}else {
				dif = 7 + desiredMonth+1; 
				for(int i=1; i<=dif; i++) { //Checks month for next year
					nextMonth.click();
				}				
			}
			clickDayDesired(activeMonthDays1, desiredDepartDay);
		}
		System.out.println("Departure and Return dates input... ");
	}	
	//Click Methods
	public void clickSearchBtn() {
		searchPckBtn.click();
	}
	
	public void clickCostsFilter() {
		costsFilter.click();
	}
	
	//Get Methods
	public List<WebElement> getOriginOptions() {
		return resultsFrom;
	}	
	
	public List<WebElement> getToOptions(){
		return resultsTo;
	}
	
	public String getCurrentMonth() {
		String currentMonth = calendarCurrentMonth.getText().split("Today is ")[1].strip().split(" ")[0];
		return currentMonth;
	}
	
	
	//Helpers
	public int sortingMonths(String monthInput) {
		String[] months = {"January", "February","March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
		int monthIndex = 0;
		for(int i=0; i<months.length; i++) {
			if(monthInput.equalsIgnoreCase(months[i])) {
				monthIndex = i;
				break;
			}
		}		
		return monthIndex;
	}
	
	public void clickDayDesired(List<WebElement> listDays, String desiredDay) {
		for(WebElement day :listDays) {
			if(day.getText().equals(desiredDay)) {
				day.click();
			}
		}
	}
}
