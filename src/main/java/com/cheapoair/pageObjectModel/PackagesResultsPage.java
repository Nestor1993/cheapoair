package com.cheapoair.pageObjectModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class PackagesResultsPage {
	
	//Class Variables
	static ArrayList<Float> listOfCosts;

	public WebDriver driver;
	
	//Constructor
	public PackagesResultsPage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver=driver;
	}
	
	
	//Locators
	@FindBy(how = How.XPATH, using="//div[@class='hotel__listing-price']/span/span[@class='priceSup']") List<WebElement> resultsCostsFilter;
	
	//Methods
	
	//Get Methods
	public List<WebElement> getCostsList() {
		return resultsCostsFilter;
	}
	
	public void printDefaultCosts(List<WebElement> defaultList)  {
		for(WebElement o :defaultList) {
			System.out.println(o.getText());
		}
	}
	
	
	public void printCosts(ArrayList<Float> arrayList) {
		for(Float counter: arrayList) {
			System.out.println(counter);
		}
	}
	
	
	//Helpers - Converts List into ArrayList	
	public ArrayList<Float> listToArray() {
		listOfCosts = new ArrayList<Float>();
		for(WebElement cost :resultsCostsFilter) {
			//converts to float String pulled, and removes extra characters
			listOfCosts.add(Float.parseFloat(cost.getText().split(" ")[0]+cost.getText().split(" ")[1])); 
		}
		//Collections.sort(listOfCosts);
		return listOfCosts;	
	}
	
	public boolean sortVerification() {
		
		boolean isMinor = true;

		for(int i=0; i<listToArray().size();i++) {
			if(listToArray().get(i)<listToArray().get(i+1)){
				isMinor = true;
				if(i+1 == listToArray().size()-1) {
					break;
				}
			}
			else {
				isMinor = false;
				break;
			}
		}
		
		return isMinor;
	}
	
	
	
	
}