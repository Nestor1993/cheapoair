package com.cheapoair.pageObjectModel;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage {
	
	WebDriver driver;
	
	//Constructor
	public void loginPage() {
		this.driver = driver;
	}
	
	
	//WebElements > Login Page
	@FindBy(how=How.XPATH, using="//input[@placeholder='Enter your Email Address']") WebElement emailField;
	@FindBy(how=How.XPATH, using="//button[contains(text(),'Proceed')]") WebElement proceedBtn;
	@FindBy(how=How.XPATH, using="//input[@placeholder='Enter Password']") WebElement passField;
	@FindBy(how=How.XPATH, using="//button[contains(text(),'Sign In')]") WebElement signInBtn;	
	@FindBy(how=How.CLASS_NAME, using="validation-error") WebElement validationErrorAlert;
	@FindBy(how=How.CLASS_NAME, using="close-icon-popup") WebElement clsWindow;	
	@FindBy(how=How.XPATH, using="//*[@class='login-popup ']/div[2]/div[2]/button") WebElement signUpBtn;
	@FindBy(how=How.CLASS_NAME, using="autosuggest-domain") WebElement autoSuggest;
	@FindBy(how=How.XPATH, using="//*[@class='autosuggest-domain']/div") WebElement autoSuggestClose;
	
	
	
	//Methods
	
	//Explicit Wait	 
	
	public WebDriverWait exWait() {
		WebDriverWait wex = new WebDriverWait(driver,5);		
		return wex;
	}
	
	//Get Methods
	public WebElement getProceedBtn() {
		return proceedBtn;
	}
	public WebElement getAutoSuggsetWElement() {
		return autoSuggest;
	}
	
	public WebElement getSingUpBtnWElement() {
		return signUpBtn;
	}

	public WebElement getValidationErrorWElement() {
		return validationErrorAlert;
	}
	
	public WebElement getPassFieldWElement() {
		return passField;
	}
	
	public String getValidationError() {
		String valError = validationErrorAlert.getText();		
		return valError;		
	}
	
	
	//Set Methods
	public void setEmailFld(String email) {
		emailField.sendKeys(email);
	}
	
	public void setPassword(String pass) {		
		passField.sendKeys(pass);
	}
	
	//Action Methods	
	public void clickProceedBtn() {		
		proceedBtn.click();		
	}
	
	public void clickSignInBtn() {
		signInBtn.click();
	}	
	
	public void closingSuccessWindow() {
		clsWindow.click();
	}
	public void closingAutoSMenu() {
		autoSuggestClose.click();
	}
	
	//Elements present in DOM: Exceptions
	public boolean isElementDisplayed(WebElement locatorElement) {
		Boolean wETrue;
		try {
			wETrue = locatorElement.isDisplayed();
			return true;
		} catch (org.openqa.selenium.NoSuchElementException e) {
			return false;
		}	
	}
}
