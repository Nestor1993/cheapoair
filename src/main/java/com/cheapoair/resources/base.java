package com.cheapoair.resources;

import java.io.FileInputStream;


import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;



public class base {
	
	
	public WebDriver driver;
	public Properties prop;
	
		
		
	
	public WebDriver initializeDriver() throws IOException
	
	{
	
		prop = new Properties();
		
		FileInputStream fis = new FileInputStream("C:\\Users\\NESTOR\\Documents\\ECLIPSE\\PROYECTOS\\Avengers\\src\\main\\java\\data.properties");
		
		prop.load(fis);
		String browsername = prop.getProperty("browser");
		String urlname = prop.getProperty("url");
		System.out.println(browsername);
		
		if (browsername.equals("Chrome"))
		{
			System.setProperty("webdriver.chrome.driver", "C:\\Users\\NESTOR\\Documents\\BROWSERS\\chromedriver.exe");
			driver = new ChromeDriver();
			
		}
		
		else if (browsername.equals("Firefox"))
		{
			System.setProperty("webdriver.gecko.driver", "C:\\Users\\NESTOR\\Documents\\SELENIUM\\geckodriver.exe");
			driver = new FirefoxDriver();
		}
		
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().deleteAllCookies();
		driver.get(urlname);
		driver.manage().window().maximize();
		return driver;
		
		
	}
	
	

}
