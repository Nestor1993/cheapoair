package com.cheapoair.utilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class DataDrivenExcel {

	//Excel file: chepoairtestdb.xlxs 
	
	
		public static ArrayList<String> getData(String xlxSheet) throws IOException{
			
			ArrayList<String> dataL = new ArrayList<String>(); //Where data from xlxs will be stored
			String srcExcelPath = System.getProperty("user.dir") + "\\src\\main\\java\\com\\cheapoair\\resources\\chepoairtestdb.xlsx";
			FileInputStream fis = new FileInputStream(srcExcelPath); //FileInputStream object created
			XSSFWorkbook workbook = new XSSFWorkbook(fis); //Create an Excel workbook object
			int sheets = workbook.getNumberOfSheets(); //# of sheets present in Excel file pulled
			
			for(int i=0; i<sheets; i++) { //this iterate across all sheets
				if(workbook.getSheetName(i).equalsIgnoreCase(xlxSheet)) { //Selects the desired Sheet, based on the input				
					XSSFSheet sheet = workbook.getSheetAt(i); //Gets Sheed index				
					//Sheet > Rows: moves between rows.
					Iterator <Row> rows = sheet.iterator(); 
					Row firstRow = rows.next(); //places the ctrl in the 1st row
					
					//Iterating through rows:
					while(rows.hasNext()) {
						Row r = rows.next();
						Iterator <Cell> cellsRow = r.cellIterator();
						while(cellsRow.hasNext()) {
							Cell c = cellsRow.next();
							if(c.getCellType()==CellType.STRING) {
								dataL.add(c.getStringCellValue());
							}						
						}				
					}
				}
			}
			return dataL;		
		}
}
