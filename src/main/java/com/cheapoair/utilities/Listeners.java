package com.cheapoair.utilities;

import java.io.IOException;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

/*
 * 
 *This class helps to handle different TestNG scenarios, based on their behavior and test events
 * 
 */

public class Listeners extends Base implements ITestListener {
	
	//Class Variables:
		int ssCount = 0;
		ExtentReports extent = ExtentReporterTestNG.getReportObject();
		ExtentTest extentTest;
		
		@Override
		public void onTestStart(ITestResult result) {
			//Extent Report Block
			extentTest = extent.createTest(getMethodName(result)); //Every time a Test starts, it will creates an Entry in this report
		}

		@Override
		public void onTestSuccess(ITestResult result) {		
			extentTest.log(Status.PASS, "Test Passed as expected");
		}

		@Override
		public void onTestFailure(ITestResult result) {
			
			//Extent Report Block
			extentTest.fail(result.getThrowable());
			
			//SS Block
			String testMethodName = getMethodName(result);  //gets method name
			try {
				extentTest.addScreenCaptureFromPath(getScreenshotPath(testMethodName, ssCount), getMethodName(result));
				ssCount++;
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println("Issue getting failure SS");
			}
		}

		@Override
		public void onTestSkipped(ITestResult result) {
			
		}

		@Override
		public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
			
		}

		@Override
		public void onStart(ITestContext context) {
			
		}

		@Override
		public void onFinish(ITestContext context) {		
			extent.flush();
		}
		
		
		//Utilities Methods:
		
		//Getting test Name/Method
		public String getMethodName(ITestResult result) {
			return result.getMethod().getMethodName();
		}
}
