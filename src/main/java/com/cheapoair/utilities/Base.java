package com.cheapoair.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
//import java.time.Duration; is throwing an error
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Base {
	
	public static WebDriver driver;
	public String baseURL;	
	public Properties prop; //@Andres: need to pull it out from the class below to make it public
	
	
	public WebDriver initializeDriver() throws IOException
	
	{		
		prop = new Properties();		
		String srcPropertiesPath = System.getProperty("user.dir") + "\\src\\main\\java\\com\\cheapoair\\resources\\data.properties"; //@Andres: added dynamic path		
		//FileInputStream fis = new FileInputStream("C:\\Users\\NESTOR\\Documents\\ECLIPSE\\PROYECTOS\\Avengers\\src\\main\\java\\data.properties");		
		FileInputStream fis = new FileInputStream(srcPropertiesPath);
		prop.load(fis);
		String browsername = prop.getProperty("browser");
		baseURL = prop.getProperty("url"); //@Andres: base url from prop file
		System.out.println(browsername);
		System.out.println(baseURL);
		
		if (browsername.equalsIgnoreCase("Chrome")) 
		{
			System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\selenium-java-3.141.59\\chromedriver_win32_90\\chromedriver.exe");
			driver = new ChromeDriver();
			
		}
		else if (browsername.equalsIgnoreCase("Firefox")){
			System.setProperty("webdriver.gecko.driver", "C:\\Selenium\\selenium-java-3.141.59\\gekodriver_win64\\geckodriver.exe");
			driver = new FirefoxDriver();
			
		}else {
			System.out.println("Invalid browser name input");
		}		
		
		driver.manage().deleteAllCookies();
		driver.get(baseURL); //@Andres: added driver.get
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		
		return driver;		
		
	}	
	
	
	//Screenshot Method:
	public String getScreenshotPath(String testCaseName, int ssVar) throws IOException {		
		TakesScreenshot ts = (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		String destinationFile = System.getProperty("user.dir")+"\\reports\\screenshots failed tests\\"+testCaseName+"-"+getTime()+"-"+"v"+ssVar+".png";
		FileUtils.copyFile(source, new File (destinationFile));
		return destinationFile;
	}
	
	

	//Elements not interactable in DOM: Exceptions
	public boolean isElementDisplayed(WebElement locatorElement) {
		Boolean wETrue;
		try {
			wETrue = locatorElement.isDisplayed();
			return true;
		} catch (org.openqa.selenium.ElementNotInteractableException e) {
			return false;
		}	
	}
	
	//Get Time/Date
	public String getTime() {
		String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
		return timeStamp;
	}
}
