package com.cheapoair.avengersTests;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.cheapoair.pageObjectModel.HomePage;
import com.cheapoair.resources.base;

public class SupportTabTest extends base {
	
	public ExcelData tc;
	public HomePage hp;
	public ExtentReports extent;
	
	@BeforeTest
	public void openBrowser() throws IOException
	{
		driver=initializeDriver();
		
		String path = System.getProperty("user.dir")+"\\reports\\index.html";
		
		ExtentSparkReporter reporter = new ExtentSparkReporter(path);
		reporter.config().setReportName("Results");
		reporter.config().setDocumentTitle("Avengers");
		
		extent = new ExtentReports();
		extent.attachReporter(reporter);
		extent.setSystemInfo("Tester", "Nestor ");
	}
	
	
	@Test
	public void ClickSupport() throws InterruptedException, IOException
	{
		
		extent.createTest("Test");
		driver.findElement(By.cssSelector("a[title='Customer Support']")).click();
		driver.findElement(By.partialLinkText("Baggage Fees")).click();
		Set<String> window = driver.getWindowHandles();
		Iterator<String> it = window.iterator();
		String Parent = it.next();
		String child = it.next();
		driver.switchTo().window(child);
		
		Thread.sleep(3000);
		driver.findElement(By.id("searchbaggage")).click();
		driver.findElement(By.id("searchbaggage")).sendKeys("Avianca");
		System.out.println(driver.findElement(By.cssSelector("div[class='hidden-sm hidden-xs col-sm-12']")).getText());
		
		WebElement avi = driver.findElement(By.cssSelector("div[class='airline xs-title-row col-xs-12 col-sm-12 col-md-1 international av ']"));
		File avisc = avi.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(avisc, new File("C:\\Users\\NESTOR\\Documents\\ECLIPSE\\PROYECTOS\\Avengers\\target\\screenshots\\Airline.jpg"));
		
		
		driver.switchTo().window(Parent);
		driver.findElement(By.cssSelector("svg[class='header-wrapper__coa']")).click();
		extent.flush();
	}

}
