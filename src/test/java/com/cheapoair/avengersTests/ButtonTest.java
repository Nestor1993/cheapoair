package com.cheapoair.avengersTests;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.cheapoair.pageObjectModel.CarsPage;
import com.cheapoair.pageObjectModel.FlightsPage;
import com.cheapoair.pageObjectModel.HomePage;
import com.cheapoair.pageObjectModel.HotelsPage;
import com.cheapoair.resources.base;

public class ButtonTest extends base {
	
	public HomePage hp;
	public CarsPage cp;
	
	@BeforeTest
	public void OpenBrowser() throws IOException
	{
		driver= initializeDriver();
	}
	
	@Test
	public void HotelButton() throws IOException
	{
		hp = new HomePage(driver);
		hp.getHotelButton().click();
		String urlhotel = driver.getCurrentUrl();
		Assert.assertEquals(urlhotel, "https://www.cheapoair.com/hotels/");
		HotelsPage hotel = new HotelsPage(driver);
		String Hotelsearch = hotel.HotelSearch().getAttribute("value");
		System.out.println(Hotelsearch);
		Assert.assertEquals(Hotelsearch, "Search Hotels");
		File screenh = hotel.HotelSearch().getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(screenh, new File("C:\\Users\\NESTOR\\Documents\\ECLIPSE\\PROYECTOS\\Avengers\\target\\screenshots\\Hotel.jpg"));
		
		
	}
	
	
	@Test(dependsOnMethods=("HotelButton"))
	public void CarsButton() throws IOException
	{
		hp = new HomePage(driver);
		hp.getCarsButton().click();
		String urlcars = driver.getCurrentUrl();
		Assert.assertEquals(urlcars, "https://www.cheapoair.com/cars/road-trip-plan-v2");
		cp = new CarsPage(driver);
		String carsearch = cp.CarSearch().getAttribute("value");
		System.out.println(carsearch);
		Assert.assertEquals(carsearch, "Search Cars");
		File screencar = cp.CarSearch().getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(screencar, new File("C:\\Users\\NESTOR\\Documents\\ECLIPSE\\PROYECTOS\\Avengers\\target\\screenshots\\Car.jpg"));
		
	
	}
	
	@Test
	public void ClickOnFlights() throws IOException
	{
		
		hp = new HomePage(driver);
		hp.getflightbutton().click();
		String urlname = driver.getCurrentUrl();
		Assert.assertEquals("https://www.cheapoair.com/flights/", urlname);
		String flight = driver.findElement(By.id("searchNow")).getAttribute("value");
		System.out.println(flight);
		Assert.assertEquals(flight, "Search Flights");		
		FlightsPage fp = new FlightsPage(driver);
		WebElement flightsearch = fp.getSearchButton();
		File screenflight = flightsearch.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(screenflight, new File ("C:\\Users\\NESTOR\\Documents\\ECLIPSE\\PROYECTOS\\Avengers\\target\\screenshots\\Flight.jpg"));
		
	}
	
	@Test(dependsOnMethods=("CarsButton"))
	public void PackageButton() throws IOException
	{
		hp = new HomePage(driver);
		hp.getPackageButton().click();
		String urlname = driver.getCurrentUrl();
		Assert.assertEquals("https://www.cheapoair.com/vacations/", urlname);
		String packagename = driver.findElement(By.id("vacationSearchBtn")).getAttribute("value");
		System.out.println(packagename);
		Assert.assertEquals(packagename, "Search Packages");
		File screenpack = hp.getPackageButton().getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(screenpack, new File("C:\\Users\\NESTOR\\Documents\\ECLIPSE\\PROYECTOS\\Avengers\\target\\screenshots\\Package.jpg"));
		
	}
	
	@AfterTest
	public void CloseDriver()
	{
		driver.close();
	}
	

}
