package com.cheapoair.avengersTests;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.cheapoair.pageObjectModel.HomePage;
import com.cheapoair.resources.base;

public class OneWayTripTest extends base {
	
	public ExcelData tc;
	public HomePage hp;
	
	@BeforeTest
	public void openBrowser() throws IOException
	{
		driver=initializeDriver();
	}
	
	
	@Test(dependsOnMethods=("ExcelDataTo"))
	public void ExcelDataFrom() throws IOException, InterruptedException
	{
		
		driver.findElement(By.cssSelector("label[for='onewayTrip']")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("from0")).click();
		ExcelData tc = new ExcelData();
		ArrayList<String> value = tc.Exceltest("City1");
		String fromvalue = value.get(1);
		driver.findElement(By.id("from0")).sendKeys(fromvalue);	
		Thread.sleep(3000);
		List<WebElement> citiex =driver.findElements(By.cssSelector("li[class='suggestion-box__item ic-flight'] span"));
		for (WebElement city : citiex)
		{
			if(city.getText().contains("Bogota"))
			{
				city.click();
				break;
			}
		}
		
		
		
	}
	
	@Test
	public void ExcelDataTo() throws IOException, InterruptedException
	{
		
		
		driver.findElement(By.id("to0")).click();
		tc = new ExcelData();
		ArrayList<String> to = tc.Exceltest("City1");
		String destino = to.get(2);
		System.out.println(destino);
		HomePage hp = new HomePage(driver);
		driver.findElement(By.id("to0")).sendKeys(destino);	
		Thread.sleep(3000);
		List<WebElement> cities =driver.findElements(By.cssSelector("li[class='suggestion-box__item ic-flight'] span"));
		for (WebElement city : cities)
		{
			if(city.getText().contains("Los Angeles"))
			{
				city.click();
				break;
			}
		}
		
		
		
	}
	
	@Test(dependsOnMethods=("ExcelDataFrom"))
	public void Passengers() throws IOException
	{
		driver.findElement(By.id("cal0")).click();
		driver.findElement(By.cssSelector("a[aria-label='24 June 2021']")).click();
		driver.findElement(By.id("travellerButton")).click();



		for(int i=1; i<=5; i++)
		{
			driver.findElement(By.id("addseniors")).click();
			driver.findElement(By.id("addchild")).click();
		}



		WebElement staticdrop = driver.findElement(By.cssSelector("select[class='form-control select-class']"));

		Select dropdown = new Select(staticdrop);

		dropdown.selectByIndex(3);
		
		hp = new HomePage(driver);
		hp.getDoneButton().click();
		

		
		
		WebElement flightsc = hp.getOneWayRange();
		
		File newsc = flightsc.getScreenshotAs(OutputType.FILE);
		
		FileUtils.copyFile(newsc, new File("C:\\Users\\NESTOR\\Documents\\ECLIPSE\\PROYECTOS\\Avengers\\target\\screenshots\\OneWay.jpg"));
		
		
	}
	
	
	
	/*@AfterTest
	public void CloseDriver()
	{
		driver.close();
	}*/
	

}
