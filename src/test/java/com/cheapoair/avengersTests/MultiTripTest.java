package com.cheapoair.avengersTests;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import com.cheapoair.pageObjectModel.HomePage;
import com.cheapoair.pageObjectModel.TravelResultsPage;
import com.cheapoair.utilities.Base;

public class MultiTripTest extends Base{
	
	//Class variables
	HomePage homePageOb;
	TravelResultsPage trResultsPageOb;
	
	//Input variables
	String from0Sug = "Medellin";
	String to0Sug = "Cartagena";
	String to1Sug = "New York";
	
	//Dates
	String desiredMonth = "October";
	String desiredMonthB = "December";
	String desiredDepartDayB = "9";
	
	static boolean varInteractable = true;
	
	
	@Test
	public void multiCityTest() throws IOException, InterruptedException {
		initializeDriver();
		//Page Factory initialization		
		homePageOb = PageFactory.initElements(driver, HomePage.class);
		trResultsPageOb = PageFactory.initElements(driver, TravelResultsPage.class);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		homePageOb.clickMultiCity();
		homePageOb.clickCloseFlight();
		homePageOb.clickOnClearFieldOrigin();
		homePageOb.setOriginFld(from0Sug);
		homePageOb.getOriginOptions().get(0).click();
		homePageOb.setDestinationFld(to0Sug);
		homePageOb.getDestinationOptions().get(0).click();
		homePageOb.clickMonthDesired(desiredMonth, "1"); 
		homePageOb.setDestinationFildB(to1Sug);
		homePageOb.getDestinationOptionsB().get(0).click();
		try {
			homePageOb.clickMonthDesiredB(desiredMonthB, desiredDepartDayB, desiredMonth);
		} catch (org.openqa.selenium.ElementNotInteractableException e) {
			System.out.println("The selected 2nd date is not available");
			varInteractable = false; 
			driver.close();
		}
		//Validates if the second Date is a valid option
		Assert.assertTrue(varInteractable);
		homePageOb.clickSearchBtn();
		//Validates number of results: > 1 means MultiTrip
		Thread.sleep(4000);
		Assert.assertEquals(trResultsPageOb.getMultiTripList().size(), 2);
		driver.close();
	}

}

