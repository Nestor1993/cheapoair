package com.cheapoair.avengersTests;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.logging.log4j.util.PropertySource.Comparator;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import com.cheapoair.pageObjectModel.HomePackagesPage;
import com.cheapoair.pageObjectModel.HomePage;
import com.cheapoair.pageObjectModel.LoginPage;
import com.cheapoair.pageObjectModel.PackagesResultsPage;
import com.cheapoair.utilities.Base;

public class PackagesTest extends Base {
	
	//Variables
	HomePage homePageOb;
	HomePackagesPage homePckOb;
	PackagesResultsPage pckResultsOb;
	
	String suggOrigin = "Bog";
	String suggDestination ="Beng";
	
	@Test
	public void packagesTest() throws IOException, InterruptedException {
		initializeDriver();
		//Page Factory initialization		
		homePageOb = PageFactory.initElements(driver, HomePage.class);
		homePckOb = PageFactory.initElements(driver, HomePackagesPage.class);
		pckResultsOb = PageFactory.initElements(driver, PackagesResultsPage.class);
		homePageOb.clickOnClearFieldOrigin();
		homePageOb.clickPackages();
		//Thread.sleep(2000);
		homePckOb.setFromTxt(suggOrigin); //Location input
		//Thread.sleep(2000);
		homePckOb.getOriginOptions().get(0).click();//clicks first item > All Location airports
		homePckOb.setToTxt(suggDestination);
		homePckOb.getToOptions().get(0).click();
		homePckOb.clickMonthDesired("February", "1"); 
		homePckOb.clickSearchBtn();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		homePckOb.clickCostsFilter();
		Thread.sleep(5000); //without this.. it pulls default costs, no order applied
		System.out.println("Displayed costs..............");
		System.out.println(pckResultsOb.getCostsList().size());
		pckResultsOb.printCosts(pckResultsOb.listToArray());
		//Validates if the following item in the list is minor than the previous one
		Assert.assertTrue(pckResultsOb.sortVerification());
		
	}
		

}

