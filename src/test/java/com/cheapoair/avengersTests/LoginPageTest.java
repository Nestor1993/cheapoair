package com.cheapoair.avengersTests;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.cheapoair.pageObjectModel.HomePage;
import com.cheapoair.pageObjectModel.LoginPage;
import com.cheapoair.utilities.Base;
import com.cheapoair.utilities.DataDrivenExcel;

public class LoginPageTest extends Base {
	
		//Variables
		LoginPage loginPageOb;
		HomePage homePageOb; 
		DataDrivenExcel excelOb;
		
		//Test Methods
		@Test(dataProvider="getData")
		public void loginTest(String userName, String pass) throws IOException, InterruptedException {		
			initializeDriver();
			homePageOb = PageFactory.initElements(driver, HomePage.class);	//Page Factory initialization	
			homePageOb.clickOnSignIn();
			loginPageOb = PageFactory.initElements(driver, LoginPage.class);	
			loginPageOb.setEmailFld(userName);		
			//Auto suggest menu handler
			if(loginPageOb.isElementDisplayed(loginPageOb.getAutoSuggsetWElement())== true) {
				loginPageOb.closingAutoSMenu();
			}
			Thread.sleep(2000);		
			loginPageOb.clickProceedBtn();		
			driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
			
			String op = " ";
			//Parameter input: String case condition and WebElement to be evaluated > output will determinate op value, based on isElementDisplayed Method
			if(loginPageOb.isElementDisplayed(loginPageOb.getPassFieldWElement()) == false){
				op = "InvalidEmailType";
				if(loginPageOb.isElementDisplayed(loginPageOb.getSingUpBtnWElement())== true) {
					op = "NewUser";			
				}
			}
			
			//Login Error/Scenario Handler
			switch(op) {
				
				case "InvalidEmailType":
					//Validates if Error alert is related to invalid Email input
					String expectedEmailAlert = "Please enter a valid email address.";
					Assert.assertEquals(loginPageOb.getValidationError(), expectedEmailAlert);			
					System.out.println("Login test - Case a1: invalid Email input");
					break;
					
				case "NewUser":
					//Validates if SingUp Button is displayed or not: means new user form was enabled
					Assert.assertTrue(loginPageOb.getSingUpBtnWElement().isDisplayed());
					System.out.println("Login test - Case a2: No registered user");
					break;
							
				default:	
					loginPageOb.setPassword(pass);
					loginPageOb.clickSignInBtn();	
					String opLog = " ";
					if(loginPageOb.isElementDisplayed(loginPageOb.getValidationErrorWElement())==true) {
						opLog = "invalidPass";
					}else {opLog = "normaLog";}
					
					switch(opLog) {
						case "invalidPass":
							//Normal Login: Password issue faced
							String expectedPwdAlert = "The email or password you entered is incorrect.";
							Assert.assertEquals(loginPageOb.getValidationError(), expectedPwdAlert);
							System.out.println("Login test - Case a3: Incorrect Password submitted");
							break;
						case "normaLog":
							//Normal Login: no User or password issues
							loginPageOb.closingSuccessWindow();
							boolean welcomeValue = homePageOb.getSuccessLogInTxt().contains("Welcome");
							Assert.assertTrue(welcomeValue);					
							System.out.println("Login test - Case a4: Succes Login test: Pass & User ok");
							break;
						default:
							System.out.print("Escenario Exception....");
							break;
					}
			}
			driver.close();
		}
		

		
		@DataProvider
		public Object[][] getData() throws IOException{
			String excelSheetDB = "User Data";
			DataDrivenExcel d = new DataDrivenExcel();		
			int count =0;
			int lSize = excelOb.getData(excelSheetDB).size()/2;		
			Object[][] dataLog = new Object[lSize][2]; //List > Array n * 2
			for(int i=0; i<lSize; i++) {
				for(int j=0; j<2; j++) {
					dataLog[i][j] = excelOb.getData(excelSheetDB).get(count); //fills array with List elements pulled from DataDrivenExcel.Class
					//System.out.print(dataLog[i][j] + " ");	
					count++; 
				}
				//System.out.println(" ");
			}
			return dataLog;
		}

}
