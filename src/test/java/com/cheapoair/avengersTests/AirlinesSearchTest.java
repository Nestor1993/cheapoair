package com.cheapoair.avengersTests;

import java.io.IOException;
//import java.time.Duration; is throwing an error
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.cheapoair.pageObjectModel.HomePage;
import com.cheapoair.pageObjectModel.TravelResultsPage;
import com.cheapoair.utilities.Base;

public class AirlinesSearchTest extends Base {

	//Class Variables
	HomePage homePageOb;
	TravelResultsPage trRePageOb;
	static final int succesRate = 15; //Succes rate is 15% which means at least top 3 results display the desired Airline
	
	//Input Variables: 
	String airlineName="Avianca Argentina";
	String suggAirlineName = "Avi";
	
	String suggOrigin = "Bog";
	String suggDestination ="Beng";
	
	int adultsInput = 4;
	int seniorsInput = 2;
	int childInput = 0;
	int seatInfatInput = 1;
	int lapInfant =2;
	String classCategory = "Business"; //Coach - Premium Economy - Business - First
	
	@Test
	public void airLinesSearch() throws IOException, InterruptedException {
		initializeDriver();		
		homePageOb = PageFactory.initElements(driver, HomePage.class);	//Page Factory initialization
		trRePageOb = PageFactory.initElements(driver, TravelResultsPage.class);	//Page Factory initialization
		Thread.sleep(2000); 
		homePageOb.clickOnClearFieldOrigin();
		homePageOb.setOriginFld(suggOrigin); //Location input
		homePageOb.getOriginOptions().get(0).click(); //clicks first item > All Location airports	
		homePageOb.setDestinationFld(suggDestination);
		homePageOb.getDestinationOptions().get(0).click(); //clicks first item > All Location airports		
		homePageOb.clickMonthDesired("October", "1"); //Desired Depart Month - Input
		Thread.sleep(2000);
		homePageOb.clickTravCoach();
		homePageOb.clickCoachMenu(adultsInput, seniorsInput, childInput, seatInfatInput, lapInfant, classCategory); //set Travel Coach quantities - Parameters: Adults // Seniors // Childs // Seat Infat // Lap Infant
		homePageOb.setAirlineSrch(suggAirlineName);
		homePageOb.clickAirlineOptions(airlineName); //verifies suggested list, and look for a match
		homePageOb.clickSearchBtn();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); //Waiting for results to load
		Thread.sleep(6000);
		Assert.assertTrue(trRePageOb.getFilterElement().isDisplayed()); //Verifies filter is displayed: 1st assertion
		Assert.assertEquals(trRePageOb.getFilterElement().getText().split(": ")[1], airlineName); //Verifies filter name match the initial input
		if(trRePageOb.getAirlinesList(airlineName) == 0) {
			System.out.println("No results found for this Airline input...");
			Assert.assertTrue(trRePageOb.getAlertNotFound().isDisplayed()); //If no results displayes, it still checks the filter works properly
		}else {
			Assert.assertTrue((trRePageOb.getAirlinesList(airlineName)/trRePageOb.getListResults().size())*100>=14); //Calculated succes rate: results over airline results
			System.out.println("Success Rate is: " + (trRePageOb.getAirlinesList(airlineName)/trRePageOb.getListResults().size())*100);
		}
		
		driver.close();
	}
}

