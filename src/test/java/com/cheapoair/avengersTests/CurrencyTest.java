package com.cheapoair.avengersTests;



import java.io.File;

import java.io.IOException;



import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;


import org.openqa.selenium.WebElement;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.cheapoair.pageObjectModel.HomePage;
import com.cheapoair.resources.base;

public class CurrencyTest extends base {
	
	@BeforeTest
	public void openBrowser() throws IOException
	{
		driver=initializeDriver();
	}
	
	
	
		
	@Test
	public void CurrencySelect() throws IOException
	{
		
			
		HomePage hp = new HomePage(driver);
		hp.getCurrency().click();
		hp.getINR().click(); 
		WebElement currency = driver.findElement(By.cssSelector("a[class='utility__link dropdown-toggle currency-arrow']"));
		File Sc = currency.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(Sc, new File ("C:\\Users\\NESTOR\\Documents\\ECLIPSE\\PROYECTOS\\Avengers\\target\\screenshots\\currency.jpg"));
		
	
	}
	
	@AfterTest
	public void CloseDriver()
	{
		driver.close();
	} 
 
}
