package com.cheapoair.avengersTests;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import com.cheapoair.pageObjectModel.HomePage;
import com.cheapoair.pageObjectModel.TravelResultsPage;
import com.cheapoair.utilities.Base;

public class RoundTripTest extends Base{
	
	//Class Variables
	HomePage homePageOb;
	TravelResultsPage trRPageOb;
	
	//Class Variables
	static boolean btnModify = false;
	
	//Test Methods
	@Test
	public void roundTripTest() throws IOException, InterruptedException {
		initializeDriver();		
		homePageOb = PageFactory.initElements(driver, HomePage.class);	//Page Factory initialization
		trRPageOb = PageFactory.initElements(driver, TravelResultsPage.class);	//Page Factory initialization
		Thread.sleep(2000);
		Assert.assertTrue(homePageOb.isCheckedRoundTrip()); //Checks if RoundTrip Radio button is checked
		System.out.println("Round Trip checked: Selecting flight...");
		homePageOb.clickOnClearFieldOrigin();
		homePageOb.setOriginFld("Lon"); //Location input
		homePageOb.getOriginOptions().get(0).click(); //clicks first item > All Location airports	
		homePageOb.setDestinationFld("Bogo");
		homePageOb.getDestinationOptions().get(0).click(); //clicks first item > All Location airports		
		homePageOb.clickMonthDesired("December", "28"); //Desired Depart Month - Input
		Thread.sleep(2000);
		homePageOb.clickTravCoach();
		homePageOb.clickCoachMenu(4, 2, 0, 1, 2, "Business"); //set Travel Coach quantities - Parameters: Adults // Seniors // Childs // Seat Infat // Lap Infant
		homePageOb.clickSearchBtn();
		driver.manage().timeouts().implicitlyWait(6, TimeUnit.SECONDS); //Waiting for results to load
		Assert.assertTrue(trRPageOb.getWebElementModifySearchBtn().isDisplayed());  
	}

}
