package com.cheapoair.avengersTests;

import java.io.FileInputStream;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.Test;

public class ExcelData {

	@Test
	public  ArrayList<String> Exceltest(String testcaseName) throws IOException {
		// TODO Auto-generated method stub
		
		
		ArrayList<String> al = new ArrayList<String>();
		
		FileInputStream fis = new FileInputStream("C:\\Users\\NESTOR\\Documents\\EXCELAVENGERS\\OneWayTrip.xlsx");
		XSSFWorkbook workbook = new XSSFWorkbook(fis);
		
		int sheets = workbook.getNumberOfSheets();
		
		for(int i=0; i<sheets; i++)
		{
			if(workbook.getSheetName(i).equalsIgnoreCase("OneWay"))
			{
				XSSFSheet selsheet = workbook.getSheetAt(i);
				
				Iterator<Row> rows = selsheet.iterator();
				
				Row first = rows.next();
				Iterator<Cell> cells =first.cellIterator();
				int k = 0;
				int column = 0;
				while(cells.hasNext())
				{
					Cell value1 = cells.next();
					if(value1.getStringCellValue().equalsIgnoreCase("Place"))
					{
						column=k;
					}
					
					k++;
				}
				
				System.out.println(column);
				
				
				while(rows.hasNext())
				{
					Row ro =rows.next();
					if(ro.getCell(column).getStringCellValue().equalsIgnoreCase(testcaseName))
					{
						Iterator<Cell> line = ro.iterator();
						while(line.hasNext())
						{
							al.add(line.next().getStringCellValue());
						}
					}
		
				}
				
				
			}
		}
		return al;
		 
 
	}

}
