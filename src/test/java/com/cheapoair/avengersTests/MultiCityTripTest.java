package com.cheapoair.avengersTests;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.cheapoair.pageObjectModel.HomePage;
import com.cheapoair.resources.base;

public class MultiCityTripTest extends base{
	
	public ExcelData tc;
	public HomePage hp;
	
	@BeforeTest
	public void openBrowser() throws IOException
	{
		driver=initializeDriver();
	}
	
	@Test(dependsOnMethods=("ExcelDataTo"))
	public void ExcelDataFrom() throws IOException, InterruptedException
	{
		
		hp.getMulti().click();
		Thread.sleep(3000);
		driver.findElement(By.id("from0")).click();
		ExcelData tc = new ExcelData();
		ArrayList<String> value = tc.Exceltest("City1");
		String fromvalue = value.get(1);
		driver.findElement(By.id("from0")).sendKeys(fromvalue);	
		Thread.sleep(3000);
		List<WebElement> citiex =driver.findElements(By.cssSelector("li[class='suggestion-box__item ic-flight'] span"));
		for (WebElement city : citiex)
		{
			if(city.getText().contains("Bogota"))
			{
				city.click();
				break;
			}
		}
		
		
		
	}
	
	@Test
	public void ExcelDataTo() throws IOException, InterruptedException
	{
		
		
		driver.findElement(By.id("to0")).click();
		tc = new ExcelData();
		ArrayList<String> to = tc.Exceltest("City1");
		String destino = to.get(2);
		System.out.println(destino);
		hp = new HomePage(driver);
		driver.findElement(By.id("to0")).sendKeys(destino);	
		Thread.sleep(3000);
		List<WebElement> cities =driver.findElements(By.cssSelector("li[class='suggestion-box__item ic-flight'] span"));
		for (WebElement city : cities)
		{
			if(city.getText().contains("Los Angeles"))
			{
				city.click();
				break;
			}
		}
		
		
		
	}
	
	@Test(dependsOnMethods=("ExcelDataFrom"))
	public void Passengers() throws IOException
	{
		driver.findElement(By.id("cal0")).click();
		driver.findElement(By.cssSelector("a[aria-label='24 June 2021']")).click();
		driver.findElement(By.id("travellerButton")).click();



		for(int i=1; i<=5; i++)
		{
			driver.findElement(By.id("addseniors")).click();
			driver.findElement(By.id("addchild")).click();
		}



		WebElement staticdrop = driver.findElement(By.cssSelector("select[class='form-control select-class']"));

		Select dropdown = new Select(staticdrop);

		dropdown.selectByIndex(3);
		
		hp = new HomePage(driver);
		hp.getDoneButton().click();
		

		
		
		WebElement flightsc = hp.getOneWayRange();
		
		File newsc = flightsc.getScreenshotAs(OutputType.FILE);
		
		FileUtils.copyFile(newsc, new File("C:\\Users\\NESTOR\\Documents\\ECLIPSE\\PROYECTOS\\Avengers\\target\\screenshots\\OneWay.jpg"));
		
		
	}
	
	@Test(dependsOnMethods= {"Passengers"})
	public void SecondCity() throws InterruptedException, IOException
	{
		
		driver.findElement(By.id("to1")).click();
		tc = new ExcelData();
		ArrayList<String> to = tc.Exceltest("City3");
		String destino = to.get(1);
		System.out.println(destino);
		hp = new HomePage(driver);
		driver.findElement(By.id("to1")).sendKeys(destino);	
		Thread.sleep(3000);
		List<WebElement> cities2 =driver.findElements(By.cssSelector("li[class='suggestion-box__item ic-flight'] span"));
		for (WebElement city2 : cities2)
		{
			if(city2.getText().contains("Armenia"))
			{
				city2.click();
				break;
			}
		}

		driver.findElement(By.cssSelector("a[aria-label='30 July 2021']")).click();

	}
	
	@Test(dependsOnMethods= {"SecondCity"})
	public void ThirdCity() throws InterruptedException, IOException
	{
		
		driver.findElement(By.id("to2")).click();
		tc = new ExcelData();
		ArrayList<String> to = tc.Exceltest("City4");
		String destino = to.get(2);
		System.out.println(destino);
		hp = new HomePage(driver);
		driver.findElement(By.id("to2")).sendKeys(destino);	
		Thread.sleep(3000);
		List<WebElement> cities3 =driver.findElements(By.cssSelector("li[class='suggestion-box__item ic-flight'] span"));
		for (WebElement city3 : cities3)
		{
			if(city3.getText().contains("Cartagena"))
			{
				city3.click();
				break;
			}
	}
		driver.findElement(By.cssSelector("a[aria-label='1 August 2021']")).click();
		driver.findElement(By.cssSelector("a[class='widget__multicity-link']")).click();
	
	}
	
	@Test(dependsOnMethods= {"ThirdCity"})
	public void FourthCity() throws InterruptedException, IOException
	{
		
		driver.findElement(By.id("to3")).click();
		tc = new ExcelData();
		ArrayList<String> to = tc.Exceltest("City4");
		String destino = to.get(1);
		System.out.println(destino);
		hp = new HomePage(driver);
		driver.findElement(By.id("to3")).sendKeys(destino);	
		Thread.sleep(3000);
		List<WebElement> cities4 =driver.findElements(By.cssSelector("li[class='suggestion-box__item ic-flight'] span"));
		for (WebElement city4 : cities4)
		{
			if(city4.getText().contains("Edinburgh"))
			{
				city4.click();
				break;
			}
		}

		Thread.sleep(3000);
		driver.findElement(By.cssSelector("a[aria-label='30 August 2021']")).click();
		Thread.sleep(3000);

		driver.findElement(By.cssSelector("a[class='widget__multicity-link']")).click();
		
	}
	
	
	@Test(dependsOnMethods= {"FourthCity"})
	public void FifthCity() throws InterruptedException, IOException
	{
		
		driver.findElement(By.id("to4")).click();
		tc = new ExcelData();
		ArrayList<String> to = tc.Exceltest("City5");
		String destino = to.get(1);
		System.out.println(destino);
		hp = new HomePage(driver);
		driver.findElement(By.id("to4")).sendKeys(destino);	
		Thread.sleep(3000);
		List<WebElement> cities5 =driver.findElements(By.cssSelector("li[class='suggestion-box__item ic-flight'] span"));
		for (WebElement city5 : cities5)
		{
			if(city5.getText().contains("Munich"))
			{
				city5.click();
				break;
			}
		}
		Thread.sleep(3000);
		driver.findElement(By.cssSelector("a[aria-label='4 September 2021']")).click();
		Thread.sleep(3000);
		driver.findElement(By.cssSelector("a[class='widget__multicity-link']")).click();
	}
	
	
	@Test(dependsOnMethods= {"FifthCity"})
	public void SixthCity() throws InterruptedException, IOException
	{
		
		driver.findElement(By.id("to5")).click();
		tc = new ExcelData();
		ArrayList<String> to = tc.Exceltest("City5");
		String destino = to.get(2);
		System.out.println(destino);
		hp = new HomePage(driver);
		driver.findElement(By.id("to5")).sendKeys(destino);	
		Thread.sleep(3000);
		List<WebElement> cities6 =driver.findElements(By.cssSelector("li[class='suggestion-box__item ic-flight'] span"));
		for (WebElement city6 : cities6)
		{
			if(city6.getText().contains("Bogota"))
			{
				city6.click();
				break;
			}
		}
		Thread.sleep(3000);
		driver.findElement(By.cssSelector("a[aria-label='10 September 2021']")).click();
		
		
		//File pagesc = driver.getScreenshotAs(OutputType.FILE);
		
		File pagesc = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		
		FileUtils.copyFile(pagesc, new File ("C:\\Users\\NESTOR\\Documents\\ECLIPSE\\PROYECTOS\\Avengers\\target\\screenshots\\MultiCity.jpg"));
		
	}
	
	
	
	
	
}
